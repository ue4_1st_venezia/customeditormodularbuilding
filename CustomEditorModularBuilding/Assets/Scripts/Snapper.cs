//using UnityEngine;
//using UnityEditor;

//public class Snapper : EditorWindow
//{
//    public float gridSize;
//    public float gridExtended;

//    SerializedObject serializeObject;
//    SerializedProperty propGridSize;
//    SerializedProperty propGridExtended;

//    private void OnEnable()
//    {
//        //serializeObject = new SerializedObject(this);
//        //propGridSize = serializeObject.FindProperty("gridSize");
//        //propGridExtended = serializeObject.FindProperty("gridExtended");
//        //gridSize = EditorPrefs.GetFloat("SNAPPER_TOOL_gridSize", 1f); //servono per salvare dati in modo semplice, non la cosa pi� flessibile del mondo
//        //gridExtended = EditorPrefs.GetFloat("SNAPPER_TOOL_gridExtended", 1f); //servono per salvare dati in modo semplice, non la cosa pi� flessibile del mondo

//       // Selection.selectionChanged += Repaint;
//       // SceneView.duringSceneGui += DuringSceneGui;
//    }

//    //private void OnDisable()
//    //{
//    //    EditorPrefs.SetFloat("SNAPPER_TOOL_gridSize", gridSize);
//    //    Selection.selectionChanged -= Repaint;
//    //    SceneView.duringSceneGui -= DuringSceneGui;
//    //}

//    //private void OnGUI()
//    //{
//    //    serializeObject.Update();
//    //    EditorGUILayout.PropertyField(propGridSize);
//    //    serializeObject.ApplyModifiedProperties();

//    //    using (new EditorGUI.DisabledScope(Selection.gameObjects.Length == 0))
//    //    {
//    //        if (GUILayout.Button("Snap Selection"))
//    //        {
//    //            SnapSelection();
//    //        }
//    //    }
//    //}

//    public Vector3 Snapping(Vector3 position)
//    {
//        serializeObject.Update();
//        EditorGUILayout.PropertyField(propGridSize);
//        EditorGUILayout.PropertyField(propGridExtended);
//        serializeObject.ApplyModifiedProperties();

//        using (new EditorGUI.DisabledScope(Selection.gameObjects.Length == 0))
//        {
//            if (GUILayout.Button("Snap Selection"))
//            {
//                SnapSelection();
//            }
//        }
//        return position;
//    }

//    private void SnapSelection()
//    {
//        foreach (GameObject gameObject in Selection.gameObjects)
//        {
//            Undo.RecordObject(gameObject.transform, "Snap Object");
//            //go.transform.position = go.transform.position.Round();
//            //gameObject.transform.position = gameObject.transform.position.Round(gridSize);
//            gameObject.transform.position = gameObject.transform.position.CenteredRound(gridSize);
//        }
//    }

//    //public void DuringSceneGui(SceneView sceneView)
//    //{
//    //    //Handles.DrawLine(Vector3.zero, Vector3.up * 5);

//    //    if (Event.current.type == EventType.Repaint)
//    //    {
//    //        Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

//    //        const float gridExtent = 16;
//    //        int lineCount = Mathf.RoundToInt((gridExtent * 2) / gridSize);

//    //        if (lineCount % 2 == 0)
//    //        {
//    //            lineCount++;
//    //        }

//    //        int halfLineCount = lineCount / 2;

//    //        for (int i = 0; i < lineCount; i++)
//    //        {
//    //            int offsetIndex = i - halfLineCount;
//    //            float xCoord = offsetIndex * gridSize;
//    //            float zCoord0 = halfLineCount * gridSize;
//    //            float zCoord1 = -halfLineCount * gridSize;
//    //            Vector3 p0 = new Vector3(xCoord, 0f, zCoord0);
//    //            Vector3 p1 = new Vector3(xCoord, 0f, zCoord1);
//    //            Handles.DrawAAPolyLine(p0, p1);

//    //            Vector3 p2 = new Vector3(zCoord0, 0f, xCoord);
//    //            Vector3 p3 = new Vector3(zCoord1, 0f, xCoord);
//    //            Handles.DrawAAPolyLine(p2, p3);
//    //        }
//    //    }
//    //}
//}