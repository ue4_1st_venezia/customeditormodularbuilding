using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class ModBuildTool : EditorWindow
{
    [MenuItem("Window/Tool/ModBuildTool")]
    public static void OpenBuildTool() => GetWindow<ModBuildTool>();

    //public
    public GameObject prefab;
    public float gridSize = 1f;
    public float gridExtent = 1f;

    //private
    private Quaternion currRot;
    private GameObject[] prefabs;
    private GameObject selectedFolder;
   
    //List
    List<GameObject> spawnPrefabs = new List<GameObject>();

    //Serialize Object
    SerializedObject serializeObject;
    SerializedProperty spPrefab;
    SerializedProperty spGridSize;
    SerializedProperty spGridExtent;
   
    private void OnEnable()
    {
        serializeObject = new SerializedObject(this);

        //FindProperty
        spPrefab = serializeObject.FindProperty("prefab");
        spGridSize = serializeObject.FindProperty("gridSize");
        spGridExtent = serializeObject.FindProperty("gridExtent");

        //EditorPrefs
        gridSize = EditorPrefs.GetFloat("BUILDTOOL_gridSize", 1f);
        gridExtent = EditorPrefs.GetFloat("BUILDTOOL_gridExtent", 1f);
        
        string[] guids = AssetDatabase.FindAssets("t:prefab", new[] { "Assets/Prefabs" });
        IEnumerable<string> paths = guids.Select(AssetDatabase.GUIDToAssetPath);
        prefabs = paths.Select(AssetDatabase.LoadAssetAtPath<GameObject>).ToArray();

        currRot = Quaternion.identity;

        SceneView.duringSceneGui += DuringSceneGUI;
    }

    private void OnDisable()
    {
        EditorPrefs.SetFloat("BUILDTOOL_gridSize", gridSize);
        EditorPrefs.SetFloat("BUILDTOOL_gridExtent", gridExtent);
       
        SceneView.duringSceneGui -= DuringSceneGUI;
    }

    private void OnGUI()
    {
        serializeObject.Update();
        EditorGUILayout.PropertyField(spPrefab);
        EditorGUILayout.PropertyField(spGridSize);
        EditorGUILayout.PropertyField(spGridExtent);

        if (serializeObject.ApplyModifiedProperties())
        {
            SceneView.RepaintAll();
        }

        if (Event.current.type == EventType.MouseDown && Event.current.button == 0)
        {
            GUI.FocusControl(null);
            Repaint();
        }
    }

    private void DuringSceneGUI(SceneView sceneView)
    {
        #region booleni nella scena per selezionare i prefab
        Handles.BeginGUI();

        Rect rect = new Rect(8, 8, 40, 40);

        foreach (GameObject prefab in prefabs)
        {
            Texture tex = AssetPreview.GetAssetPreview(prefab);

            if (GUI.Toggle(rect, this.prefab == prefab, new GUIContent(tex)))
            {
                this.prefab = prefab;
                Repaint();
            }            
           
            if (EditorGUI.EndChangeCheck())
            {
                spawnPrefabs.Clear();

                for (int i = 0; i < prefabs.Length; i++)
                {
                    spawnPrefabs.Add(prefabs[i]);
                }
            }

            rect.y += rect.height + 2;
        }

        Handles.EndGUI();
        #endregion

        RotateObject();

        //disegnamo la griglia
        DrawGrid();

        Transform camTf = sceneView.camera.transform;

        if (Event.current.type == EventType.MouseMove)
        {
            sceneView.Repaint();
        }

        Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

        if (Physics.Raycast(ray, out RaycastHit hit) && hit.collider != null)
        {

            //disegnamo la normale 
            Handles.DrawAAPolyLine(5, hit.point, hit.point + hit.normal);

            //spawniamo i prefab
            if (hit.collider.CompareTag("Floor"))
            {
                //disegnamo i prefab
                DrawObjects(sceneView, hit);

                if (Event.current.keyCode == KeyCode.Space && Event.current.type == EventType.KeyDown)
                {
                    SpawnObject(hit);
                }
            }
        }
    }

    #region SpawnObject
    private void SpawnObject(RaycastHit hit)
    {
        if (prefab == null) return;

        selectedFolder = GameObject.FindGameObjectWithTag("Folder");

        if (selectedFolder == null)
        {
            selectedFolder = new GameObject("FloorFolder");
            selectedFolder.tag = "Folder";
        }

        GameObject spawnedItem = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
        spawnedItem.transform.SetParent(selectedFolder.transform);

        //dato che la rotazione dell'oggetto ha come normale l'asse y e non la z dobbiamo ruotarte l'oggetto per farlo combaciare con la nostra asse z
        Vector3 pos = (hit.point + hit.normal * .1f).Round();

        Quaternion randomRot = Quaternion.Euler(0, 0, 0); //lo si mette alla z perch� ora essa corrisponde alla y degli oggetti
        Quaternion rot = Quaternion.identity * currRot;
        spawnedItem.transform.position = pos;
        spawnedItem.transform.rotation = rot;

        Undo.RegisterCreatedObjectUndo(spawnedItem, "Prefab");

    }
    #endregion

    #region Grid
    private void DrawGrid()
    {
        if (Event.current.type == EventType.Repaint)
        {
            Handles.zTest = UnityEngine.Rendering.CompareFunction.LessEqual;

            int lineCount = Mathf.RoundToInt((gridExtent * 2) / gridSize);

            if (lineCount % 2 == 0)
            {
                lineCount++;
            }

            int halfLineCount = lineCount / 2;

            for (int i = 0; i < lineCount; i++)
            {
                int offsetIndex = i - halfLineCount;
                float xCoord = offsetIndex * gridSize + 0.5f;
                float zCoord0 = halfLineCount * gridSize + 0.5f;
                float zCoord1 = -halfLineCount * gridSize + 0.5f;

                Vector3 point0 = new Vector3(xCoord, 0f, zCoord0);
                Vector3 point1 = new Vector3(xCoord, 0f, zCoord1);
                Handles.DrawAAPolyLine(point0, point1);

                Vector3 point2 = new Vector3(zCoord0, 0f, xCoord);
                Vector3 point3 = new Vector3(zCoord1, 0f, xCoord);
                Handles.DrawAAPolyLine(point2, point3);
            }
        }
    }
    #endregion

    #region DrawObjects
    private void DrawObjects(SceneView sceneView, RaycastHit hit)
    {
        Matrix4x4 pointToWordlMatrix = Matrix4x4.TRS(hit.point.Round(), Quaternion.identity * currRot, Vector3.one);
        MeshFilter[] filters = prefab.GetComponentsInChildren<MeshFilter>();

        foreach (MeshFilter filter in filters)
        {
            Matrix4x4 childToPoint = filter.transform.localToWorldMatrix;
            Matrix4x4 childToWorldMtx = pointToWordlMatrix * childToPoint;
            Mesh mesh = filter.sharedMesh;
            Material mat = filter.GetComponent<MeshRenderer>().sharedMaterial;
            mat.SetPass(0);
            Graphics.DrawMesh(mesh, childToWorldMtx, mat, 0, sceneView.camera);
        }
    }
    #endregion

    #region RotateObject
    private void RotateObject()
    {
        if (Event.current.keyCode == KeyCode.R && Event.current.type == EventType.KeyDown) //R = Rotate
        {
            currRot *= Quaternion.Euler(0f, 90f, 0f);
        }
        else if (Event.current.keyCode == KeyCode.I && Event.current.type == EventType.KeyDown) //I = Inverse
        {
            currRot *= Quaternion.Euler(0f, -90f, 0f);
        }
    }
    #endregion
}
