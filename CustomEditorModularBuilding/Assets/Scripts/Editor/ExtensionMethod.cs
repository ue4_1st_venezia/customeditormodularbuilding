using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class ExtensionMethod 
{
    public static Vector3 Round(this Vector3 v)
    {
        v.x = Mathf.Round(v.x);
        v.y = Mathf.Round(v.y) + .5f;
        v.z = Mathf.Round(v.z);
        
        return v;
    }

    public static Vector3 CenteredRound(this Vector3 v, float step)
    {
        Vector3 x;
        return x = (v / step).Round() * step;
    }    

}
